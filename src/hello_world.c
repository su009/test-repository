#include <stdio.h>

#define HELLO_MESSAGE "Hello world\n"

int main(int argc, char** argv) {
	printf(HELLO_MESSAGE);
	return 0;
}
